<html>
  <head>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  </head>

  <body class="grey lighten-2">
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>

<?php

	// turn on errors for Heroku hosting debug stuff
	error_reporting(-1);
	ini_set('display_errors', 1);
	ini_set('html_errors', 1);

	// Make sure on initial page load we were sent by POST
	if (!($_SERVER['REQUEST_METHOD'] === 'POST')) {
		header("Location: index.php");
	}

	$server = $_POST['server'];
	$database = $_POST['database'];
	$username = $_POST['username'];
	$password = $_POST['password'];
	$type = $_POST['type'];

	// Ensure that all necessary data isn't null
	if (!($server && $database && $username && $password && $type)) {
		// TODO: add error before redirect
		header("Location: index.php");
	}

	// load our session
	session_start();
	$_SESSION['server'] = $server;
	$_SESSION['database'] = $database;
	$_SESSION['username'] = $username;
	$_SESSION['password'] = $password;
	$_SESSION['type'] = $type;

	$db = null;
	switch($type) {
	  case "sqlsrv":
	    $db = new PDO($type . ':Server=' . $server . ';Database=' . $database, $username, $password);
	    break;
	  case "mysql":
	    $db = new PDO($type . ':host=' . $server . ';dbname=' . $database . ';charset=utf8', $username, $password);
	    break;
      case "pgsql":
      	$db = new PDO($type . ':host=' . $server . ';port=5432;dbname=' . $database . ';user=' . $username . ';password=' . $password);
      	break;
      	//pgsql:host=localhost;port=5432;dbname=testdb;user=bruce;password=mypass
	}

	// grab all tables in database for display
	$counter = 0;
	$tables = array();
	switch($type) {
		case "pgsql":
			foreach($db->query("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = 'public'") as $row) {
				array_push($tables, $row['table_name']);
			}
			break;
		default:
			foreach($db->query("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES") as $row) {
				array_push($tables, $row['TABLE_NAME']);
			}
			break;
	}
	// DEBUG stuff
	// echo "Successfully connected to a database<br>" 
	// 	. "Server: {$server} <br>"
	// 	. "Database: {$database} <br>"
	// 	. "Username: {$username} <br>"
	// 	. "Password: {$password} <br>"
	// 	. "Type: {$type}";

		// $key = hash('md5', 'k0m2pyZq7JnTgXfO6eIC');
		// $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
  //   	$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		// $encryptedPassword = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key,
  //                                $password, MCRYPT_MODE_CBC, $iv);
		// $ciphertext = $iv . $encryptedPassword;
  //   	$ciphertext_base64 = base64_encode($ciphertext);
?>

  <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper">
        <?php echo "Server: {$server} | Database: {$database} | Type: {$type}"; ?>
      </div>
    </nav>
  </div>
  <div class="row">
    <br/>
    <div class="col s10">
      <div class="row">
        <div class="col s10">
          <textarea class="form-control" name="query" id="queryTA" rows="5" cols="25"></textarea>
        </div>
        <div class="col s2">
          <button class="btn btn-primary" id="queryButton">query</button>
        </div>
      </div>
      <div id="returnData"></div>
    </div>

    <div class="col s2">
      <h2>Tables</h2>
      <?php foreach ($tables as $table) { ?>
        <ul <?php echo 'id="table' . $counter . '"'; ?> class="dropdown-content table-options" table="<?php echo $table ?>">
          <li><a href="#!" type="select">Select<i class="material-icons right">search</i></a></li>
          <li><a href="#!" type="update">Update<i class="material-icons right">mode_edit</i></a></li>
          <li><a href="#!" type="insert">Insert<i class="material-icons right">input</i></a></li>
          <li><a href="#!" type="delete">Delete<i class="material-icons right">delete</i></a></li>
          <li><a href="#!" type="drop">Drop<i class="material-icons right">system_update_alt</i></a></li>
        </ul>
        <a class="btn dropdown-button" href="#!" <?php echo 'data-activates="table' . $counter . '">' . $table; ?>
          <i class="mdi-navigation-arrow-drop-down right"></i>
        </a>
        <br />
        <br />
        <?php $counter++; ?>
      <?php } ?>
      <button id="newTableButton" type="button" class="btn btn-default">+</button>
    </div>
  </div>
<script src="console.js" type="text/javascript"></script>