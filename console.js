var srvVal = $('#hiddenServer').text();
var dbVal = $('#hiddenDatabase').text();
var userVal = $('#hiddenUsername').text();
var passVal = $('#hiddenPassword').text();
var typeVal = $('#hiddenType').text();

$("#queryButton").click(function() {
	// query database
	$.ajax({
		type: "POST",
		url: "dal.php",
		data: { srv: srvVal,
				db: dbVal,
				user: userVal,
				pass: passVal,
				type: typeVal,
				test: 'testing',
				action: 'runQuery',
				query: $('#queryTA').val() },
		context: document.body,
		success: function(response) {
			$("#returnData").html(response);
		}
	});
});

function setQuery(text) {
	$("#queryTA").val(text);
}

$(".table-options a").click(function(e) {
	var table = $(e.target).closest('ul').attr('table');
	switch($(e.target).attr('type')) {
		case 'select':
			setQuery("SELECT * FROM [" + table + "];");
			break;
		case 'update':
			setQuery("UPDATE " + table + " SET column_name1 = value1 WHERE column_name2 = value2;");
			break;
		case 'delete':
			setQuery("DELETE FROM " + table + " WHERE column_name1 = value1;");
			break;
		case 'drop':
			setQuery("DROP TABLE " + table + ";");

	}
});

$("#newTableButton").click(function() {
	setQuery("CREATE TABLE table_name ( column_name1 data_type1, column_name2 data_type2 );");
});