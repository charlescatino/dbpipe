<?php
	$pgsqlGetTablesQuery = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = 'public'";
	$sqlsrvGetTablesQuery = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES";

	session_start();
	$srv = $_SESSION['server']; //$_POST['srv'];
	$db = $_SESSION['database']; //$_POST['db'];
	$user = $_SESSION['username']; //$_POST['user'];
	$pass = $_SESSION['password']; //$_POST['pass'];
	$type = $_SESSION['type']; //$_POST['type'];
	$action = $_POST['action'];

	// DEBUG
	echo $srv;
	echo '<br />';
	echo $db;
	echo '<br />';
	echo $user;
	echo '<br />';
	echo $pass;
	echo '<br />';
	echo $action;
	echo '<br />';
	echo $type;
	echo '<br />';

	$dbo = null;
	$dbo = establishConnection();

	switch($action) {
		case 'getTables':
			getTables();
			break;
		case 'runQuery';
			runQuery($_POST['query']);
			break;
		default:
			break;
	}

	function getTables() {
		global $dbo, $type;
		switch($type) {
		case "pgsql":
			foreach($dbo->query($pgsqlGetTablesQuery) as $row) {
				array_push($tables, $row['table_name']);
			}
			break;
		default:
			foreach($dbo->query($sqlsrvGetTablesQuery) as $row) {
				array_push($tables, $row['TABLE_NAME']);
			}
			break;
		}
	}

	function runQuery($query) {
		global $dbo;
		echo "My query is $query<br>";
		try {
			$result = $dbo->prepare($query);
			$result->execute();
			$pos = stripos($query, 'select');
			if ($pos !== false) {
				query($query, $result);
			} else {
				update($query);
			}
		} catch(PDOException $ex) {
			echo "An Error has occurred!" . $ex;
			// TODO: log me
		}
	}

	// PRIVATE FUNCTIONS

	function query($sql, $result) {
		$cols = $result->columnCount();
		echo "<table class=\"table\"><tr>";
		for ($i = 0; $i < $cols; $i++) {
		  $meta_arr = $result->getColumnMeta($i);
			echo "<th>" . $meta_arr["name"] . "</th>";
		}
		echo "</tr>";
		while ($row = $result->fetch(PDO::FETCH_NUM)) {
			echo "<tr>";
			for ($i = 0; $i < $cols; $i++) {
				echo "<td>" . $row[$i] . "</td>";
			}
			echo "</tr>";
		}
		echo "</table>";
	}

	function update($sql) {
		echo "Query \"<i>" . $sql . "</i>\"" . " executed successfully.";
	}

	function establishConnection() {
		global $srv, $db, $user, $pass, $type;
		switch($type) {
		  case "sqlsrv":
		    $dbo = new PDO($type . ':Server=' . $srv . ';Database=' . $db, $user, $pass);
		    break;
		  case "pgsql":
		    $dbo = new PDO($type . ':host=' . $srv . ';port=5432;dbname=' . $db . ';user=' . $user . ';password=' . $pass);
	      	break;
		  default:
		    $dbo = new PDO($type . ':host=' . $srv . ';dbname=' . $db . ';charset=utf8', $user, $pass);
		    break;
		}
		$dbo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $dbo;
	}
?>