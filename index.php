<html>
  <head>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>

  <body class="grey lighten-2">
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>

    <div class="container">
      <div class="row">
        <h2 class="center-align">DBPipe</h2>
      </div>
      <div class="row">
        <div class="col s12 m12 l8 offset-l2">
          <div class="card-panel">
            <form name="credentials" action="console.php" method="post">  
              <div class="input-field col s12">
                <input id="serverTB" name="server" type="text" class="validate">
                <label for="serverTB">Server Name</label>
              </div>
              <div class="input-field col s12">
                <input id="databaseTB" name="database" type="text" class="validate">
                <label for="databaseTB">Database Name</label>
              </div>
              <div class="input-field col s12">
                <input id="usernameTB" name="username" type="text" class="validate">
                <label for="usernameTB">Username</label>
              </div>
              <div class="input-field col s12">
                <input id="passwordTB" name="password" type="password" class="validate">
                <label for="passwordTB">Password</label>
              </div>
              <div class="input-field col s12">
                <select name="type">
                  <option value="" disabled selected>Select a database type...</option>
                  <option value="sqlsrv">SQL Server</option>
                  <option value="pgsql">Postgress</option>
                  <option value="mysql">MySQL</option>
                  <option value="4">Nope</option>
                </select>
              </div>
              <button class="btn-large waves-effect waves-light" type="submit" name="action">Connect
                <i class="material-icons right">send</i>
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>

<script type="text/javascript">
    $(document).ready(function() {
      $('select').material_select();
    })
</script>

<?php
if ($_SERVER['SERVER_NAME'] === "localhost") { ?>
  <h1>Stuff below here only shows up in localhost</h1>
  <button id="testButton" value="test credentials">SQL Server test DB</button>
  <button id="pgsqlTestButton">PGSQL test DB (old lanreg)</button>
  <script type="text/javascript">
    $('#testButton').click(function() {
      $('#serverTB').val('rosebloom.arvixe.com');
      $('#databaseTB').val('dbpipetest');
      $('#usernameTB').val('db_pipe');
      $('#passwordTB').val('testing');
    });
    $('#pgsqlTestButton').click(function() {
      $('#serverTB').val('ec2-54-225-236-102.compute-1.amazonaws.com');
      $('#databaseTB').val('d9uekhmj238nh');
      $('#usernameTB').val('oynkzukugbhdxv');
      $('#passwordTB').val('e0a133a0f1be07fc9d46fcd86a48b6e4256b5bb1525cfe7f7f9185acaa9d81f8');
    })
  </script>
<?php } ?>